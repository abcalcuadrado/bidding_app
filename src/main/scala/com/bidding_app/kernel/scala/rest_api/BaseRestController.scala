package com.bidding_app.kernel.scala.rest_api

import akka.actor.ActorSystem
import akka.event.{Logging, LoggingAdapter}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.StandardRoute
import akka.stream.ActorMaterializer
import com.google.gson.Gson
import spray.json.DefaultJsonProtocol
import java.util


abstract class BaseRestController(implicit s: ActorSystem, m: ActorMaterializer)
  extends Config with SprayJsonSupport with DefaultJsonProtocol {
  protected def serviceName: String = this.getClass.getSimpleName

  protected def system: ActorSystem = s

  protected def materializer: ActorMaterializer = m

  protected def log: LoggingAdapter = Logging(system, serviceName)

  def responseComplete[T](data: T): StandardRoute = {
    StandardRoute(_.complete(new Gson toJson new BaseResponse(data)))
  }

}
