package com.bidding_app.kernel.scala.rest_api

/**
  * Created by xavier on 4/6/16.
  */
class BaseResponse[T](val data: T)
