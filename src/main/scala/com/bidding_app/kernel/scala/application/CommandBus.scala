package com.bidding_app.kernel.scala.application

import akka.actor.Actor.Receive
import akka.actor.{Props, Actor, ActorSystem}
import akka.util.Timeout
import com.bidding_app.microservice.core.application.chat_session.command.{CreateChatSessionASCmd, ChatSessionCAS}

import scala.collection.mutable
import scala.reflect.ClassTag
import scala.concurrent.Future
import scala.reflect._
import akka.pattern.ask
import scala.concurrent.duration._


class CommandBus[T <: ApplicationServiceCommand]

object CommandBus {
  var handlerMap = mutable.Map(CreateChatSessionASCmd.toString -> ChatSessionCAS.Create)

  class CommandBusActor[T <: ApplicationServiceCommand, V] extends Actor {
      override def receive: Receive = {
      case CommandBusActorMsg(handlerName, command) =>
        val x = handlerMap.get(handlerName)
        if (x.isDefined) {
          val handler = x.get.asInstanceOf[ApplicationService[T]]
          sender() ! handler.handle(command.asInstanceOf[T]).asInstanceOf[V]
          println("en otro ldado")
        } else {
          throw new Exception("No handler")
        }
    }
  }

  case class CommandBusActorMsg(handlerName: String, command: Any)


  implicit class ImplicitHandler[T <: ApplicationServiceCommand : ClassTag](ref: CommandBus[T]) {
    def handle[U](f: U ⇒ T)(implicit system: ActorSystem): Future[U] = {
      implicit val timeout = Timeout(25 seconds)
      val actorRef = system.actorOf(Props(new CommandBusActor[T, U]))
      val future = actorRef ? CommandBusActorMsg(classTag[T].runtimeClass.getSimpleName, f(null.asInstanceOf[U]))
      future.asInstanceOf[Future[U]]
    }
  }


  def addHandler(commandType: String, handler: Any): Unit = {

    //        handlerMap.+=(commandType,handler.asInstanceOf[Handler[Cmd]])
  }
}
