package com.bidding_app.kernel.scala.application

import scala.concurrent.Future

/**
  * Created by xavier on 3/31/16.
  */
trait ApplicationService[T <: ApplicationServiceCommand] {

  def handle(command: T): T#R
}