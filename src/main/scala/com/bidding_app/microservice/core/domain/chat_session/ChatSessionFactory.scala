package com.bidding_app.microservice.core.domain.chat_session

import akka.actor.{ActorSystem, Props}
import com.bidding_app.kernel.scala.domain.AggregateFactory

/**
  * Created by xavier on 3/30/16.
  */
class ChatSessionFactory(implicit as: ActorSystem)  extends AggregateFactory[ChatSessionAggregate] {
  override def props(id: String): Props = ChatSessionAggregate.props(id)
}
