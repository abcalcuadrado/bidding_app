package com.bidding_app.microservice.core.domain.chat_session

import akka.actor.Props
import akka.persistence.SnapshotMetadata
import com.bidding_app.kernel.scala.domain.AggregateRoot
import com.bidding_app.kernel.scala.domain.AggregateRoot.{Event, State}

/**
  * Created by xavier on 3/30/16.
  */
object ChatSessionAggregate {

  import AggregateRoot._

  case class ChatSession(id: String) extends State

  case class Initialize(pass: String) extends Command

  case class UserInitialized(pass: String) extends Event

  def props(id: String): Props = Props(new ChatSessionAggregate(id))
}


class ChatSessionAggregate(id: String) extends AggregateRoot {
  override def persistenceId: String = id

  override protected def restoreFromSnapshot(metadata: SnapshotMetadata, state: State): Unit = ???

  /**
    * Updates internal processor state according to event that is to be applied.
    *
    * @param evt Event to apply
    */
  override def updateState(evt: Event): Unit = ???

  override def receiveCommand: Receive = ???

}
