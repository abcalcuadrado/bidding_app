package com.bidding_app.microservice.core.application.chat_session.command

import akka.actor.ActorSystem
import com.bidding_app.kernel.scala.application.{ApplicationService, ResourceDTO}
import com.bidding_app.microservice.core.domain.chat_session.ChatSessionFactory


/**
  * Created by xavier on 3/31/16.
  */

object ChatSessionCAS {

  object Create extends ApplicationService[CreateChatSessionASCmd] {
    override def handle(command: CreateChatSessionASCmd): ResourceDTO = {
      implicit val system: ActorSystem = ActorSystem()
//      new ChatSessionFactory().findOrCreate(command.listingId.toString)
      new ResourceDTO(1, ",", 1)
    }

  }
}


