package com.bidding_app.microservice.core.application.chat_session.query

import akka.actor.ActorSystem
import com.bidding_app.kernel.scala.application.{ApplicationService, ResourceDTO}
import com.bidding_app.microservice.core.domain.chat_session.ChatSessionFactory
import com.bidding_app.microservice.core.infrastructure.dao.AuctionDAO


/**
  * Created by xavier on 3/31/16.
  */

object ChatSessionQAS {


  def findByUserId = {
    new AuctionDAO findByUserId()
  }


}


