package com.bidding_app.microservice.core.application.chat_session.command

import com.bidding_app.kernel.scala.application.{ApplicationServiceCommand, ResourceDTO}

/**
  * Created by xavier on 3/31/16.
  */
trait ChatSessionASCmd extends ApplicationServiceCommand

case class CreateChatSessionASCmd(listingId: Long,
                                  leadContactId: Long)
                                 (response: ResourceDTO) extends ChatSessionASCmd {
  override type R = ResourceDTO
}