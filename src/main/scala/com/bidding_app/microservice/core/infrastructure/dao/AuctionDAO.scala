package com.bidding_app.microservice.core.infrastructure.dao

import java.util

import com.sksamuel.elastic4s.{ElasticsearchClientUri, ElasticClient}
import org.elasticsearch.common.settings.Settings
import com.sksamuel.elastic4s.ElasticDsl._

/**
  * Created by xavier on 4/6/16.
  */
class AuctionDAO {

  val client = ElasticClient.transport(Settings.builder.put("cluster.name", "elasticsearch_xavier").build, ElasticsearchClientUri("127.0.0.1", 9300))

  def findByUserId(): Array[util.Map[String, AnyRef]] = {

    client.execute {
      search in "auctions" / "all"
    }.await.getHits.getHits.map(_.sourceAsMap())

  }

}
