package com.bidding_app.microservice.core.infrastructure.to

/**
  * Created by xavier on 4/2/16.
  */
case class AuctionTO(id: String,
                     brand: String,
                     model: String,
                     image: String,
                     price: BigDecimal,
                     isNew: Boolean,
                     owner: String,
                     isFromOfficialOwner: Boolean,
                     currentOffer: BigDecimal,
                     yourOffer: BigDecimal,
                     isFavourite: Boolean,
                     lastOfferAt: Long,
                     version: Int
                    )

