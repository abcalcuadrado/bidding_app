package com.bidding_app.microservice.core.rest_api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.bidding_app.kernel.scala.rest_api.Config
import com.bidding_app.microservice.core.rest_api.controller.v1.ChatSessionController

object Main extends App with Config {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer: ActorMaterializer = ActorMaterializer()


  val chatSessionControllerV1 = new ChatSessionController()

  Http().bindAndHandle(chatSessionControllerV1.routes, httpInterface, httpPort)
}
