package com.bidding_app.microservice.core.rest_api.controller.v1

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.bidding_app.kernel.scala.application.CommandBus
import com.bidding_app.kernel.scala.rest_api.BaseRestController
import com.bidding_app.kernel.scala.application.CommandBus.ImplicitHandler
import com.bidding_app.microservice.core.application.chat_session.command.CreateChatSessionASCmd
import com.bidding_app.microservice.core.application.chat_session.query.ChatSessionQAS
import com.google.gson.Gson
import concurrent.ExecutionContext.Implicits.global
import spray.json._
import com.bidding_app.kernel.scala.application.ResourceDTO


/**
  * Created by xavier on 3/31/16.
  */
class ChatSessionController(implicit system: ActorSystem, materializer: ActorMaterializer)
  extends BaseRestController {

  implicit val usersFormat = jsonFormat3(ResourceDTO)

  val routes =
    pathPrefix("v1") {

      path("chat-sessions") {
        get {
          responseComplete {
            ChatSessionQAS.findByUserId
            //            new CommandBus[CreateChatSessionASCmd] handle CreateChatSessionASCmd(1, 2) map (_.toJson)

          }
        }

      }
    }

}
