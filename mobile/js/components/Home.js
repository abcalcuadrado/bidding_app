/**
 * Created by jorge on 26/01/16.
 */
'use strict';
import React, {Component, ScrollView, Text, StyleSheet, View} from 'react-native';
import SearchBar from './SearchBar'
import CreditsAlert from './CreditsAlert'
import AuctionCard from './AuctionCard'

export default class Home extends Component {

    constructor(props) {
        super(props)
    }

    /*
     AuctionCard.propTypes = {header: React.PropTypes.string};
     AuctionCard.propTypes = {vendor: React.PropTypes.string};
     AuctionCard.propTypes = {isOfficialVendor: React.PropTypes.bool};
     AuctionCard.propTypes = {isOpen: React.PropTypes.bool};
     AuctionCard.propTypes = {price: React.PropTypes.number};
     AuctionCard.propTypes = {currentOffer: React.PropTypes.number};
     AuctionCard.propTypes = {yourOffer: React.PropTypes.number};
     AuctionCard.propTypes = {imgSrc: React.PropTypes.string};
     */

    render() {
        return (
            <View style={styles.container}>
                <SearchBar />
                <ScrollView style={styles.cardScrollView}>

                    <CreditsAlert amount={45}/>
                    <AuctionCard header="IPhone 6s 32GB"
                                 vendor="BiddingApp"
                                 isOfficialVendor={true}
                                 isOpen={true}
                                 price={1000}
                                 currentOffer={30}
                                 yourOffer={25}
                                 imgSrc="https://d3nevzfk7ii3be.cloudfront.net/igi/Ct5US1pWPUJq1nJg.standard"
                                 timeForClosing={90}/>
                    <AuctionCard header="IPhone 6s 32GB"
                                 vendor="BiddingApp"
                                 isOfficialVendor={true}
                                 isOpen={true}
                                 price={1000}
                                 currentOffer={30}
                                 yourOffer={35}
                                 imgSrc="https://d3nevzfk7ii3be.cloudfront.net/igi/Ct5US1pWPUJq1nJg.standard"
                                 timeForClosing={0}/>
                    <AuctionCard header="IPhone 6s 32GB"
                                 vendor="BiddingApp"
                                 isOfficialVendor={true}
                                 isOpen={false}
                                 price={1000}
                                 currentOffer={30}
                                 yourOffer={25}
                                 imgSrc="https://d3nevzfk7ii3be.cloudfront.net/igi/Ct5US1pWPUJq1nJg.standard"
                                 timeForClosing={0}/>
                    <View style={styles.separator}/>


                </ScrollView>
            </View>
        );
    }
}
var styles = StyleSheet.create({
    cardScrollView: {
        backgroundColor: "#EFEFEF",
        flexDirection: 'column',
        flex: 1

    },
    container: {
        flexDirection: 'column',
        flex: 1,
        marginBottom: 50
    },
    separator:{
        height: 10
    }
});