import React, {Component, View, Text, AsyncStorage} from 'react-native';
import {Router, Route, Scene} from 'react-native-router-flux'
import Splash from '../components/Splash';
import Home from '../components/Home';
import * as Routes from '../constants/Routes'
var Icon = require('react-native-vector-icons/MaterialIcons')


class TabIcon extends React.Component {
    constructor(props) {
        super(props);
    }

    getIconName(name) {
        console.log(this.props)
        switch (name) {
            case Routes.HOME:
                return "whatshot";
            case Routes.MY_AUCTIONS:
                return "loyalty";
            case Routes.ACCOUNT:
                return "person";
            case Routes.NOTIFICATIONS:
                return "notifications";
            default:
                return "search";


        }
    }

    render() {

        return (
            <Icon name={this.getIconName(this.props.name)} size={30} color={this.props.selected?"#F3F3F3":"#90CAF9"}/>
        );
    }
}
class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Scene key="tabbar" tabs={true} default={Routes.HOME} initial={true} tabBarStyle={{padding: 0, margin: 0, backgroundColor: "#1976D2"}}>
                    <Scene key={Routes.HOME} component={Home} icon={TabIcon} hideNavBar={true}/>
                    <Scene key={Routes.MY_AUCTIONS} component={Home} icon={TabIcon} hideNavBar={true}/>
                    <Scene key={Routes.ACCOUNT} component={Home} icon={TabIcon} hideNavBar={true}/>
                    <Scene key={Routes.NOTIFICATIONS} component={Home} icon={TabIcon} hideNavBar={true}/>
                </Scene>

            </Router>
        );
    }
}


module.exports = App;