import React, {Component, View, Text, StyleSheet, TextInput} from 'react-native';
var Icon = require('react-native-vector-icons/MaterialIcons')


export default class SearchBar extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.searchBox}>
                    <Icon name="search" size={25} color="#7D7D7D"/>
                    <TextInput
                        style={styles.searchText}
                        onChangeText={(text) => this.setState({text})}
                        onSubmitEditing={this.props.onSearchSubmit}
                        placeholder="Busca por marca, modelo y más"
                        placeholderTextColor="#BCBCBC"
                    />
                </View>

            </View>

        )
        // <View style={styles.filterIconContainer}>
        //     <Icon name="filter-list" size={30} color="#FFFFFF"/>
        // </View>
    }
}

var styles = StyleSheet.create({
    container: {
        height: 85,
        backgroundColor: '#1976D2',
        paddingHorizontal: 14,
        paddingBottom: 12,
        paddingTop: 30,
        flexDirection: 'row',
        justifyContent: 'flex-start'
    },
    searchBox: {
        backgroundColor: '#FFFFFF',
        borderRadius: 3,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        height: 40,
        flex: 1,
        flexDirection: 'row',
        padding: 7
    },
    searchText: {
        flex: 1,
        marginLeft: 5,
        marginTop: 2,
        fontFamily: "Roboto-Thin",



    },
    filterIconContainer: {
        paddingLeft: 10,
        paddingVertical: 7
    }
});