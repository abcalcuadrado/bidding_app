import React, {Component, View, Text, StyleSheet, TextInput} from 'react-native';
var Icon = require('react-native-vector-icons/MaterialIcons')


export default class CreditsAlert extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            this.props.amount > 0 ?
                <View style={styles.container}>
                    <View >
                        <View style={styles.flexWrapper}>
                            <Text style={styles.normalText}>Aprovecha tus créditos en </Text>
                            <Text style={styles.boldText}>compras directas, </Text>
                            <Text style={styles.normalText}>tienes </Text>
                            <Text style={styles.boldText}>${this.props.amount} </Text>
                            <Text style={styles.boldText}>en créditos </Text>
                        </View>
                    </View>
                </View> :
                null
        )
    }


}

var styles = StyleSheet.create({
    container: {
        backgroundColor: "#fff",
        padding: 5,
        paddingHorizontal: 10,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: .4,
        shadowColor: "#ccc",
        marginTop: 10,
    },
    flexWrapper: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // height: 24
    },
    boldText: {
        fontFamily: "Roboto-Regular",
        fontSize: 17
    },
    normalText: {
        fontFamily: "Roboto-Thin",
        fontSize: 17
    }
});


CreditsAlert.propTypes = {amount: React.PropTypes.number};
CreditsAlert.defaultProps = {amount: 0};