import React, {Component, View, Text, StyleSheet, Image} from 'react-native';
var Icon = require('react-native-vector-icons/MaterialIcons')

const PRICE = "PRECIO"
const YOUR_OFFER = "TU OFERTA"
const CURRENT_OFFER = "OFERTA ACTUAL"
const WINNER_OFFER = "OFERTA GANADORA"

export default class AuctionCard extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let userIsWinning = this.props.currentOffer <= this.props.yourOffer && this.props.isOpen;
        let userIsLosing = this.props.currentOffer > this.props.yourOffer && this.props.isOpen;
        let isOpen = this.props.isOpen;
        return (
            <View style={styles.container}>
                <View style={styles.productBox}>

                    <Image
                        style={styles.photo}
                        source={{uri: this.props.imgSrc}}
                    />
                    <View style={styles.details}>
                        <View style={styles.detailsHeader}>
                            <Text style={styles.detailsHeaderText}>{this.props.header}</Text>
                            <View style={styles.headerIconsWrapper}>
                                <Icon name="notifications-none" size={24} color="#666666" style={styles.icon}/>
                                <Icon name="share" size={24} color="#666666" style={styles.icon}/>
                            </View>
                        </View>
                        <View style={styles.vendorInfo}>
                            <Text style={styles.vendorInfoText}>por {this.props.vendor}</Text>
                            {this.props.isOfficialVendor ? <Icon name="check-circle" size={15} color="#2196F3"
                                                                 style={styles.vendorInfoChecked}/> : null}
                        </View>
                        <View style={styles.priceInfo}>
                            <View style={styles.priceItemContainerFirstBox}>
                                <View style={styles.priceItemContainer}>
                                    <Text style={styles.priceItemText}>{PRICE}</Text>
                                </View>
                                {userIsWinning ?
                                    <View style={styles.priceItemContainer}>
                                        <Text style={styles.yourOfferItemTextWinning}>{YOUR_OFFER}</Text>
                                    </View> :
                                    null}
                                {userIsLosing ?
                                    <View style={styles.priceItemContainer}>
                                        <Text style={styles.yourOfferItemTextLosing}>{YOUR_OFFER}</Text>
                                    </View> :
                                    null}
                                <View style={styles.priceItemContainer}>
                                    <Text
                                        style={styles.currentOfferItemText}>{isOpen ? CURRENT_OFFER : WINNER_OFFER}</Text>
                                </View>
                            </View>
                            <View>
                                <View style={styles.priceItemContainer}>
                                    <Text style={styles.priceItemAmount}>${this.props.price}</Text>
                                </View>
                                {userIsWinning ?
                                    <View style={styles.priceItemContainer}>
                                        <Text style={styles.yourOfferItemAmountWinning}>${this.props.yourOffer}
                                            Ganando</Text>
                                    </View> :
                                    null}
                                {userIsLosing ?
                                    <View style={styles.priceItemContainer}>
                                        <Text style={styles.yourOfferItemAmountLosing}>${this.props.yourOffer} </Text>
                                    </View> :
                                    null}
                                <View style={styles.priceItemContainer}>
                                    <Text style={styles.currentOfferItemAmount}>${this.props.currentOffer} </Text>
                                </View>
                            </View>


                        </View>
                    </View>
                </View>
                <View style={styles.actionsContainer}>
                    <View style={styles.actionButton}>
                        <Text style={styles.leftActionButtonText}>COMPRA DIRECTA</Text>
                    </View>
                    <View style={styles.separator}/>
                    {isOpen ?
                        <View style={styles.actionButton}>
                            <Text style={styles.rightActionButtonTextOpen}>OFERTAR</Text>
                            <Text style={styles.actionButtonHelper}>Termina en {this.props.timeForClosing} seg</Text>
                        </View> :
                        <View style={styles.actionButton}>
                            <Text style={styles.rightActionButtonTextClosed}>SUBASTADO</Text>
                            <Text style={styles.actionButtonHelper}>Atento al siguiente</Text>
                        </View>
                    }
                </View>

            </View>
        )
    }


}

var styles = StyleSheet.create({
    container: {
        flexDirection: 'column',
        height: 208,
        backgroundColor: "#FFFFFF",
        marginTop: 10,
        shadowOffset: {width: 0, height: 2},
        shadowOpacity: .4,
        shadowColor: "#bbb"
    },
    productBox: {
        flexDirection: 'row',
        flex: 1,
        paddingTop: 10,
        paddingHorizontal: 15
    },
    photo: {
        width: 100,
        height: 130
    },
    details: {
        flexDirection: 'column',
        flex: 1,
        paddingLeft: 15
    },
    detailsHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 30
    },
    detailsHeaderText: {
        fontFamily: "Roboto-Regular",
        fontSize: 22,
        color: "#434343"
    },
    icon: {
        marginLeft: 3
    },
    headerIconsWrapper: {
        flexDirection: 'row'
    },
    vendorInfo: {
        flexDirection: 'row'
    },
    vendorInfoText: {
        fontFamily: "Roboto-Thin",
        color: "#666666",
        fontSize: 18

    },
    vendorInfoChecked: {
        marginLeft: 4,
        alignSelf: 'flex-end',
        marginBottom: 2
    },
    priceInfo: {
        flexDirection: 'row',
        paddingTop: 7
    },
    priceItemContainerFirstBox: {
        width: 130
    },
    actionsContainer: {
        flexDirection: 'row',
        height: 53,
    },
    priceItemContainer: {
        flexDirection: 'row',
        height: 23,
        alignItems: 'flex-end'
    },
    priceItemText: {
        color: '#666',
        fontFamily: "Roboto-Light",
        fontSize: 13
    },
    priceItemAmount: {
        color: '#666',
        fontFamily: "Roboto-Light",
        fontSize: 15
    },
    yourOfferItemTextWinning: {
        fontSize: 13,
        color: '#4CAF50',
        fontFamily: "Roboto-Light"
    },
    yourOfferItemAmountWinning: {
        color: '#4CAF50',
        fontFamily: "Roboto-Light",
        fontSize: 15
    },
    yourOfferItemTextLosing: {
        color: '#F44336',
        fontFamily: "Roboto-Light",
        fontSize: 13
    },
    yourOfferItemAmountLosing: {
        color: '#F44336',
        fontFamily: "Roboto-Light",
        fontSize: 15
    },
    currentOfferItemText: {
        color: '#1976D2',
        fontFamily: "Roboto-Light",
        fontSize: 13
    },
    currentOfferItemAmount: {
        color: '#1976D2',
        fontFamily: "Roboto-Light",
        fontSize: 15
    },
    actionButton: {
        backgroundColor: "#F7F7F7",
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        flexDirection: 'column'
    },
    leftActionButtonText: {
        color: "#808080",
        fontFamily: "Roboto-Light",
        fontSize: 17
    },
    rightActionButtonTextOpen: {
        color: "#434343",
        fontFamily: "Roboto-Regular",
        fontSize: 17
    },
    rightActionButtonTextClosed: {
        color: "#666666",
        fontFamily: "Roboto-Regular",
        fontSize: 17
    },
    separator: {
        width: 2
    },
    actionButtonHelper: {
        color: "#999999",
        fontFamily: "Roboto-Light",
        fontSize: 14,
        lineHeight: 17
    }
});


AuctionCard.propTypes = {
    header: React.PropTypes.string.isRequired,
    vendor: React.PropTypes.string.isRequired,
    isOfficialVendor: React.PropTypes.bool.isRequired,
    isOpen: React.PropTypes.bool.isRequired,
    price: React.PropTypes.number.isRequired,
    currentOffer: React.PropTypes.number.isRequired,
    yourOffer: React.PropTypes.number.isRequired,
    timeForClosing: React.PropTypes.number.isRequired,
    imgSrc: React.PropTypes.string.isRequired
};

AuctionCard.defaultProps = {
    isOpen: false,
    yourOffer: 0,
    currentOffer: 0
};