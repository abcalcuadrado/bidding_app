name := """microservice-motor-chat"""

organization := "com.github.pvoznenko"

version := "0.0.1"

scalaVersion := "2.11.6"

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

libraryDependencies ++= {
  val akkaStreamVersion = "1.0-RC4"
  val akkaVersion = "2.3.11"
  val akkaPersistenceVersion = "2.4.2"
  val guiceVersion = "4.0"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream-experimental" % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-http-experimental" % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-http-core-experimental" % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-http-testkit-experimental" % akkaStreamVersion,
    "com.typesafe.akka" %% "akka-persistence" % akkaPersistenceVersion,
    "org.scalatest" %% "scalatest" % "2.2.5" % "test",
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % "test",
    "com.google.inject" % "guice" % guiceVersion,
    "io.spray" %% "spray-json" % "1.3.2",
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % akkaStreamVersion,
    "com.typesafe.play" %% "play-json" % "2.5.1",
    "org.mongodb" %% "casbah" % "3.1.1",
    "com.github.scullxbones" %% "akka-persistence-mongo-casbah" % "1.2.2",
    "com.sksamuel.elastic4s"%%"elastic4s-core"%"2.3.0",
    "com.google.code.gson"%"gson"%"2.6.2"

  )
}

Revolver.settings


fork in run := true