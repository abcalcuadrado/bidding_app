import React, {Component, View, Text, AsyncStorage} from 'react-native';
import {Router, Route} from 'react-native-router-flux'
import Splash from '../components/Splash';
import Home from '../components/Home';
import * as Routes from '../constants/Routes'


class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Route name={Routes.SPLASH} component={Splash} initial={true}
                       hideNavBar={true}
                       title="Splash" />
                <Route name={Routes.HOME} component={Home}
                       title="Home" hideNavBar={true}/>
            </Router>
        );
    }
}
module.exports = App;

