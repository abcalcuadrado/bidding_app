/**
 * Created by jorge on 26/01/16.
 */
'use strict';
var React= require('react-native');
var {
    StyleSheet,
    Text,
    TextInput,
    View,
    TouchableHighlight,
    ActivityIndicatorIOS,
    Image,
    Component
} = React;

class Home extends Component {

    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View >
                <Text>Home</Text>
            </View>
        );
    }
}
module.exports = Home;