/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import App from 'js/components/App'
import React, {
  AppRegistry
} from 'react-native';


AppRegistry.registerComponent('mobile', () => App);
